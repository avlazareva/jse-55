package ru.t1.lazareva.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbstractFieldException {

    @SuppressWarnings("unused")
    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    @SuppressWarnings("unused")
    public NumberIncorrectException(@NotNull final String value, @NotNull final Throwable cause) {
        super("Error! This value \"" + value + "\" is incorrect...");
    }

}