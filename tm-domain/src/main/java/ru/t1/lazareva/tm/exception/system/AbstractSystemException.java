package ru.t1.lazareva.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.exception.AbstractException;

public abstract class AbstractSystemException extends AbstractException {

    @SuppressWarnings("unused")
    public AbstractSystemException() {
    }

    public AbstractSystemException(@NotNull final String message) {
        super(message);
    }

    @SuppressWarnings("unused")
    public AbstractSystemException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    @SuppressWarnings("unused")
    public AbstractSystemException(@NotNull final Throwable cause) {
        super(cause);
    }

    @SuppressWarnings("unused")
    public AbstractSystemException(@NotNull final String message, @NotNull final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}