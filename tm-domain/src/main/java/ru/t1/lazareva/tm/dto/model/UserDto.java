package ru.t1.lazareva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity
@Table(name = "tm.tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDto extends AbstractModelDto {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "login")
    private String login;

    @NotNull
    @Column(nullable = false, name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column(nullable = true, name = "email")
    private String email;

    @Nullable
    @Column(nullable = true, name = "first_name")
    private String firstName;

    @Nullable
    @Column(nullable = true, name = "last_name")
    private String lastName;

    @Nullable
    @Column(nullable = true, name = "middle_name")
    private String middleName;

    @NotNull
    @Column(nullable = false, name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false, name = "locked")
    private Boolean locked = false;

    public UserDto(@NotNull final String login, @Nullable final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDto(@NotNull final String login, @NotNull final String passwordHash, @Nullable final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public UserDto(@NotNull final String login, @NotNull final String passwordHash, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

}
