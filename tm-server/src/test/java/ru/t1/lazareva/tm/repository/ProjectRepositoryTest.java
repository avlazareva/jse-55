package ru.t1.lazareva.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.lazareva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.configuration.ServerConfiguration;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;

import javax.persistence.EntityManager;

import static ru.t1.lazareva.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private static final ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IPropertyService propertyService = CONTEXT.getBean(IPropertyService.class);

    @NotNull
    public static IProjectDtoRepository getRepository() {
        return CONTEXT.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    private static final IProjectDtoRepository REPOSITORY = getRepository();

    @Nullable
    private final static EntityManager ENTITY_MANAGER = REPOSITORY.getEntityManager();

    @NotNull
    private static final IUserDtoService USER_SERVICE = CONTEXT.getBean(IUserDtoService.class);

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @BeforeClass
    @SneakyThrows
    public static void beforeClazz() {
        USER_SERVICE.add(USER_1);
        USER_SERVICE.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        USER_SERVICE.remove(USER_1);
        USER_SERVICE.remove(USER_2);
        ENTITY_MANAGER.close();
    }

    @Before
    @SneakyThrows
    public void before() {
        if (ENTITY_MANAGER.getTransaction().isActive())
            ENTITY_MANAGER.getTransaction().rollback();
    }

    @After
    @SneakyThrows
    public void after() {
        for (@NotNull final ProjectDto project : PROJECT_LIST) {
            try {
                ENTITY_MANAGER.getTransaction().begin();
                REPOSITORY.remove(project);
                ENTITY_MANAGER.getTransaction().commit();
            } catch (@NotNull final Exception e) {
                ENTITY_MANAGER.getTransaction().rollback();
            }
        }
        try {
            ENTITY_MANAGER.getTransaction().begin();
            REPOSITORY.clear(USER_1.getId());
            ENTITY_MANAGER.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            ENTITY_MANAGER.getTransaction().rollback();
        }
        try {
            ENTITY_MANAGER.getTransaction().begin();
            REPOSITORY.clear(USER_2.getId());
            ENTITY_MANAGER.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            ENTITY_MANAGER.getTransaction().rollback();
        }
    }

    @Test
    public void add() {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_PROJECT1);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final ProjectDto project = REPOSITORY.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findAll() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_PROJECT1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(REPOSITORY.findAll());
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final ProjectDto createdProject = USER_PROJECT1;
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(createdProject);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertFalse(REPOSITORY.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(REPOSITORY.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        @NotNull final ProjectDto createdProject = USER_PROJECT1;
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(createdProject);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertFalse(REPOSITORY.existsById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(REPOSITORY.existsById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_PROJECT1);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final ProjectDto project1 = REPOSITORY.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(USER_PROJECT1.getId(), project1.getId());
        @Nullable final ProjectDto project2 = REPOSITORY.findOneById("");
        Assert.assertNull(project2);
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        @NotNull final ProjectDto createdProject = USER_PROJECT1;
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1.getId(), createdProject);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNull(REPOSITORY.findOneById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDto project = REPOSITORY.findOneById(USER_1.getId(), createdProject.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(createdProject, project);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(createdProject);
        ENTITY_MANAGER.getTransaction().commit();
    }

    @Test
    public void clear() throws Exception {
        @NotNull final ProjectDto createdProject = USER_PROJECT1;
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(createdProject);
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.clear(USER_PROJECT1.getUserId());
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(0, REPOSITORY.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_PROJECT1);
        REPOSITORY.add(USER_PROJECT2);
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.clear(USER_1.getId());
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(0, REPOSITORY.getSize(USER_1.getId()));
    }

    @Test
    public void remove() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final ProjectDto createdProject = REPOSITORY.add(USER_PROJECT1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdProject);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(createdProject);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final ProjectDto project = REPOSITORY.findOneById(USER_PROJECT1.getId());
        Assert.assertNull(project);
    }

    @Test
    public void removeByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final ProjectDto createdProject = REPOSITORY.add(USER_PROJECT1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdProject);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(USER_1.getId(), createdProject);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final ProjectDto project = REPOSITORY.findOneById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNull(project);
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final ProjectDto createdProject = REPOSITORY.add(USER_PROJECT1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdProject);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.removeById(USER_1.getId(), USER_PROJECT1.getId());
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final ProjectDto project = REPOSITORY.findOneById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNull(project);
    }

    @Test
    public void getSize() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.clear();
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertTrue(REPOSITORY.findAll().isEmpty());
        Assert.assertEquals(0, REPOSITORY.getSize());
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_PROJECT1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(1, REPOSITORY.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.clear();
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertTrue(REPOSITORY.findAll().isEmpty());
        Assert.assertEquals(0, REPOSITORY.getSize(USER_PROJECT1.getUserId()));
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_PROJECT1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(1, REPOSITORY.getSize(USER_PROJECT1.getUserId()));
    }

}
