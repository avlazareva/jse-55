package ru.t1.lazareva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Status;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class TaskTestData {

    @Nullable
    public final static String USER_1_LOGIN = "test";

    @Nullable
    public final static String USER_1_PASSWORD = "test";

    @Nullable
    public final static String USER_1_EMAIL = "test@test.ru";

    @Nullable
    public final static String USER_2_LOGIN = "admin";

    @Nullable
    public final static String USER_2_PASSWORD = "admin";

    @Nullable
    public final static String USER_2_EMAIL = "admin@admin.ru";

    @NotNull
    public final static UserDto USER_1 = new UserDto(USER_1_LOGIN, USER_1_PASSWORD, USER_1_EMAIL);
    @NotNull
    public final static TaskDto USER_TASK1 = new TaskDto(USER_1, "task", "123", Status.NOT_STARTED);
    @NotNull
    public final static UserDto USER_2 = new UserDto(USER_2_LOGIN, USER_2_PASSWORD, USER_2_EMAIL);
    @NotNull
    public final static TaskDto USER_TASK2 = new TaskDto(USER_2, "task2", "123", Status.COMPLETED);
    @NotNull
    public final static List<TaskDto> TASK_LIST = Arrays.asList(USER_TASK1, USER_TASK2);
    @Nullable
    public final static TaskDto NULL_TASK = null;
    @NotNull
    public final static String NON_EXISTING_TASK_ID = UUID.randomUUID().toString();

}