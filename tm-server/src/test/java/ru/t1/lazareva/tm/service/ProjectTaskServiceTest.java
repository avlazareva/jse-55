package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.ITaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.configuration.ServerConfiguration;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.lazareva.tm.exception.entity.TaskNotFoundException;
import ru.t1.lazareva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.lazareva.tm.exception.field.TaskIdEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;
import ru.t1.lazareva.tm.service.dto.ProjectDtoService;
import ru.t1.lazareva.tm.service.dto.ProjectTaskDtoService;
import ru.t1.lazareva.tm.service.dto.TaskDtoService;
import ru.t1.lazareva.tm.service.dto.UserDtoService;

import javax.persistence.EntityManager;

import static ru.t1.lazareva.tm.constant.ProjectTestData.NON_EXISTING_PROJECT_ID;
import static ru.t1.lazareva.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1.lazareva.tm.constant.TaskTestData.NON_EXISTING_TASK_ID;
import static ru.t1.lazareva.tm.constant.TaskTestData.USER_TASK1;
import static ru.t1.lazareva.tm.constant.UserTestData.USER_1;
import static ru.t1.lazareva.tm.constant.UserTestData.USER_2;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private static final IProjectDtoService PROJECT_SERVICE = CONTEXT.getBean(IProjectDtoService.class);

    @NotNull
    private static final IProjectTaskDtoService SERVICE = CONTEXT.getBean(IProjectTaskDtoService.class);

    @NotNull
    private static final ITaskDtoService TASK_SERVICE = CONTEXT.getBean(ITaskDtoService.class);

    @NotNull
    private static final IUserDtoService USER_SERVICE = CONTEXT.getBean(IUserDtoService.class);

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() {
        USER_SERVICE.add(USER_1);
        USER_SERVICE.add(USER_2);
    }

    @After
    public void after() throws Exception {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        USER_SERVICE.clear();
    }

    @Test
    public void bindTaskToProject() throws Exception {
        PROJECT_SERVICE.add(USER_1.getId(), USER_PROJECT1);
        TASK_SERVICE.add(USER_1.getId(), USER_TASK1);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(USER_1.getId(), null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(USER_1.getId(), "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            SERVICE.bindTaskToProject(USER_1.getId(), NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            SERVICE.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        SERVICE.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDto task = TASK_SERVICE.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT1.getId(), task.getProjectId());
    }

    @Test
    public void removeProjectById() throws Exception {
        PROJECT_SERVICE.add(USER_1.getId(), USER_PROJECT1);
        TASK_SERVICE.add(USER_1.getId(), USER_TASK1);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeProjectById(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeProjectById("", USER_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.removeProjectById(USER_1.getId(), null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.removeProjectById(USER_1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            SERVICE.removeProjectById(USER_1.getId(), NON_EXISTING_PROJECT_ID);
        });
        SERVICE.bindTaskToProject(USER_1.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        SERVICE.removeProjectById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            PROJECT_SERVICE.findOneById(USER_PROJECT1.getId());
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            TASK_SERVICE.findOneById(USER_TASK1.getId());
        });
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        PROJECT_SERVICE.add(USER_1.getId(), USER_PROJECT1);
        TASK_SERVICE.add(USER_1.getId(), USER_TASK1);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(USER_1.getId(), null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(USER_1.getId(), "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(USER_1.getId(), USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(USER_1.getId(), USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            SERVICE.unbindTaskFromProject(USER_1.getId(), NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            SERVICE.unbindTaskFromProject(USER_1.getId(), USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        SERVICE.unbindTaskFromProject(USER_1.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDto task = TASK_SERVICE.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
    }

}
