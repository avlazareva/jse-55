package ru.t1.lazareva.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.lazareva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.configuration.ServerConfiguration;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;

import javax.persistence.EntityManager;
import java.util.Collections;

import static ru.t1.lazareva.tm.constant.ProjectTestData.NON_EXISTING_PROJECT_ID;
import static ru.t1.lazareva.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private static final ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IPropertyService propertyService = CONTEXT.getBean(IPropertyService.class);

    @NotNull
    public static ISessionDtoRepository getRepository() {
        return CONTEXT.getBean(ISessionDtoRepository.class);
    }

    @NotNull
    private static final ISessionDtoRepository REPOSITORY = getRepository();

    @Nullable
    private final static EntityManager ENTITY_MANAGER = REPOSITORY.getEntityManager();

    @NotNull
    private static final ISessionDtoRepository EMPTY_REPOSITORY = getRepository();

    @NotNull
    private static final IUserDtoService USER_SERVICE = CONTEXT.getBean(IUserDtoService.class);

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @BeforeClass
    @SneakyThrows
    public static void beforeClazz() {
        USER_SERVICE.add(USER_1);
        USER_SERVICE.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        USER_SERVICE.remove(USER_1);
        USER_SERVICE.remove(USER_2);
        ENTITY_MANAGER.close();
    }

    @Before
    @SneakyThrows
    public void before() {
        if (ENTITY_MANAGER.getTransaction().isActive())
            ENTITY_MANAGER.getTransaction().rollback();
    }

    @After
    @SneakyThrows
    public void after() {
        for (@NotNull final SessionDto session : USER_SESSION_LIST) {
            try {
                ENTITY_MANAGER.getTransaction().begin();
                REPOSITORY.remove(session);
                ENTITY_MANAGER.getTransaction().commit();
            } catch (@NotNull final Exception e) {
                ENTITY_MANAGER.getTransaction().rollback();
            }
        }

        try {
            ENTITY_MANAGER.getTransaction().begin();
            REPOSITORY.clear(USER_1.getId());
            ENTITY_MANAGER.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            ENTITY_MANAGER.getTransaction().rollback();
        }

        try {
            ENTITY_MANAGER.getTransaction().begin();
            REPOSITORY.clear(USER_2.getId());
            ENTITY_MANAGER.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            ENTITY_MANAGER.getTransaction().rollback();
        }
    }

    @Test
    public void add() {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add((USER_1_SESSION));
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final SessionDto session = REPOSITORY.findOneById(USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    public void findAll() {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(REPOSITORY.findAll());
    }

    @Test
    public void findAllByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(REPOSITORY.findAll());
        Assert.assertEquals(Collections.emptyList(), REPOSITORY.findAll(""));
        Assert.assertNotNull(REPOSITORY.findAll(USER_1_SESSION.getUserId()));
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final SessionDto createdSession = USER_1_SESSION;
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.add(createdSession);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertFalse(REPOSITORY.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(REPOSITORY.existsById(USER_1_SESSION.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        @NotNull final SessionDto createdSession = USER_1_SESSION;
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.add(createdSession);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertFalse(REPOSITORY.existsById(USER_1.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(REPOSITORY.existsById(USER_1.getId(), createdSession.getId()));
    }

    @Test
    public void findOneById() {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final SessionDto session1 = REPOSITORY.findOneById(USER_1_SESSION.getId());
        Assert.assertNotNull(session1);
        Assert.assertEquals(USER_1_SESSION.getId(), session1.getId());
        @Nullable final SessionDto session2 = REPOSITORY.findOneById("");
        Assert.assertNull(session2);
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        @NotNull final SessionDto createdSession = USER_1_SESSION;
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1.getId(), createdSession);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNull(REPOSITORY.findOneById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final SessionDto session = REPOSITORY.findOneById(USER_1.getId(), createdSession.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(createdSession, session);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(createdSession);
        ENTITY_MANAGER.getTransaction().commit();
    }

    @Test
    public void clear() throws Exception {
        @NotNull final SessionDto createdProject = USER_1_SESSION;
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.add(createdProject);
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.clear(USER_1_SESSION.getUserId());
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(0, EMPTY_REPOSITORY.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.add(USER_1_SESSION);
        EMPTY_REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.clear(USER_1.getId());
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(0, EMPTY_REPOSITORY.getSize(USER_1.getId()));
    }

    public void remove() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final SessionDto createdSession = REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdSession);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(createdSession);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final SessionDto session = REPOSITORY.findOneById(USER_1_SESSION.getId());
        Assert.assertNull(session);
    }

    @Test
    public void removeByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final SessionDto createdSession = REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdSession);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(USER_1.getId(), createdSession);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final SessionDto session = REPOSITORY.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNull(session);
    }

    public void removeById() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final SessionDto createdSession = REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdSession);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.removeById(USER_1.getId(), createdSession.getId());
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final SessionDto session = REPOSITORY.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNull(session);
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final SessionDto createdSession = REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdSession);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.removeById(USER_1.getId(), USER_1_SESSION.getId());
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final SessionDto session = REPOSITORY.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNull(session);
    }

    @Test
    public void getSize() throws Exception {
        Assert.assertTrue(EMPTY_REPOSITORY.findAll().isEmpty());
        Assert.assertEquals(0, EMPTY_REPOSITORY.getSize());
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(1, EMPTY_REPOSITORY.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertTrue(EMPTY_REPOSITORY.findAll().isEmpty());
        Assert.assertEquals(0, EMPTY_REPOSITORY.getSize(USER_1_SESSION.getUserId()));
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.add(USER_1_SESSION);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(1, EMPTY_REPOSITORY.getSize(USER_1_SESSION.getUserId()));
    }

}
