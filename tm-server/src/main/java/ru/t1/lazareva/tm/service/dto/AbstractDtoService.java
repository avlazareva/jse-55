package ru.t1.lazareva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.repository.dto.IDtoRepository;
import ru.t1.lazareva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.lazareva.tm.api.service.dto.IDtoService;
import ru.t1.lazareva.tm.dto.model.AbstractModelDto;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import java.util.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoService<M extends AbstractModelDto, R extends IDtoRepository<M>> implements IDtoService<M> {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract IDtoRepository<M> getRepository();

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        @NotNull final IDtoRepository<M> modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<M> resultModels = new ArrayList<>();
        @NotNull final IDtoRepository<M> modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final M model : models) {
                @NotNull final M resultModel = modelRepository.add(model);
                resultModels.add(resultModel);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModels;
    }

    @Override
    public void clear() throws Exception {
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsByIndex(@Nullable final Integer index) throws Exception {
        if (index == null) return false;
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }


    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            resultModel = repository.findOneById(id);
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            resultModel = repository.findOneByIndex(index);
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws Exception {
        @Nullable M result = findOneById(id);
        remove(result);
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) {
        @Nullable M result = findOneByIndex(index);
        remove(result);
    }

    @Override
    public M update(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        @NotNull final IDtoRepository<M> modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        @Nullable M resultModel;
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;

    }

}
