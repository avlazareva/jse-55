package ru.t1.lazareva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull EntityManager getEntityManager();

    @NotNull
    M add(@NotNull M model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull Integer index) throws Exception;


    int getSize() throws Exception;

    void remove(@NotNull M model) throws Exception;

    void removeById(@NotNull String id) throws Exception;

    @Nullable
    M update(@NotNull M model) throws Exception;

}