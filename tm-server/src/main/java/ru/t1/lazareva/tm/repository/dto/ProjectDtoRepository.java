package ru.t1.lazareva.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.lazareva.tm.dto.model.ProjectDto;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> implements IProjectDtoRepository {

    @Override
    protected Class<ProjectDto> getEntityClass() {
        return ProjectDto.class;
    }

    @NotNull
    @Override
    public ProjectDto create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final ProjectDto project = new ProjectDto(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public ProjectDto create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final ProjectDto project = new ProjectDto(name);
        return add(userId, project);
    }

}


