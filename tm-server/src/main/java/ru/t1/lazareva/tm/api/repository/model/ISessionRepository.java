package ru.t1.lazareva.tm.api.repository.model;

import ru.t1.lazareva.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}

