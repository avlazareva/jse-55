package ru.t1.lazareva.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.lazareva.tm.api.service.IAuthService;
import ru.t1.lazareva.tm.api.service.IServiceLocator;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.dto.request.AbstractUserRequest;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.exception.EndpointException;
import ru.t1.lazareva.tm.exception.user.AccessDeniedException;

@Controller
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    private IAuthService authService;

    protected SessionDto check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        try {
            @NotNull final SessionDto session = authService.validateToken(token);
            if (session.getRole() == null) throw new AccessDeniedException();
            if (!session.getRole().equals(role)) throw new AccessDeniedException();
            return session;
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    protected SessionDto check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        try {
            return authService.validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

}
