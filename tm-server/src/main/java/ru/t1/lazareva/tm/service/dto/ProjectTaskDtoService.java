package ru.t1.lazareva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.lazareva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.ITaskDtoService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.lazareva.tm.exception.entity.TaskNotFoundException;
import ru.t1.lazareva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.lazareva.tm.exception.field.TaskIdEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.repository.dto.ProjectDtoRepository;
import ru.t1.lazareva.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private IProjectDtoRepository getProjectRepository() {
        return context.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    private ITaskDtoRepository getTaskRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDto task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            @Nullable ProjectDto project = projectRepository.findOneById(userId, projectId);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final List<TaskDto> tasks = taskRepository.findAllByProjectId(userId, projectId);
            if (tasks == null) throw new TaskNotFoundException();
            entityManager.getTransaction().begin();
            for (final TaskDto task : tasks) taskRepository.removeById(userId, task.getId());
            projectRepository.removeById(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDto task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}