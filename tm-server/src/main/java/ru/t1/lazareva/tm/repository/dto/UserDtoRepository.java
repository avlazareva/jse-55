package ru.t1.lazareva.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Role;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    @Override
    protected Class<UserDto> getEntityClass() {
        return UserDto.class;
    }

    @NotNull
    @Override
    public UserDto create(@NotNull final String login, @NotNull final String password) throws Exception {
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) throws Exception {
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    public UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) throws Exception {
        @NotNull final UserDto user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public UserDto findByLogin(@NotNull final String login) throws Exception {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.login = :login";
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("login", login)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@NotNull final String email) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.email = :email";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("email", email)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExists(@NotNull final String login) throws Exception {
        return findByLogin(login) != null;
    }

    @Override
    public Boolean isEmailExists(@NotNull final String email) throws Exception {
        return findByEmail(email) != null;
    }

}


