# TASK MANAGER

## DEVELOPER INFO

* **Name**: Anna Lazareva

* **E-mail**: alazareva@t1-consulting.ru

* **E-mail**: anyutasolomatina22@gmail.com

## SOFTWARE

* **OS**: WINDOWS 10

* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: i5

* **RAM**: 16GB

* **SSD**: 512GB

## PROGRAM BUILD

``` shell
mvn clean install
```

## PROGRAM RUN

``` shell
java -jar ./task-manager.jar
```