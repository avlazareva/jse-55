package ru.t1.lazareva.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.api.model.ICommand;
import ru.t1.lazareva.tm.api.service.ITokenService;

@Component
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract String getName();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        String result = "";

        final boolean hasName = name != null && !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();

        if (hasName) result += name;
        if (hasArgument) result += hasName ? ", " + argument : argument;
        if (hasDescription) result += ": " + description;
        return result;
    }

}