package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getAdminLogin();

    @NotNull
    String getAdminPassword();

    @NotNull String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLogs();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getPort();

    @NotNull
    String getHost();

}
